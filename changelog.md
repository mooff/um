# v0.1.4 (5/24/2023)

- publish to PyPI under GPLv3

# v0.1.2 (5/12/2023)

- combine suggestions from previously corrected scripts and newly generated scripts
- explain scripts on demand
